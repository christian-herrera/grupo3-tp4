#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <string.h>
#include <ctype.h>
#include <sys/unistd.h>
#include <signal.h>

/* ============================================> Definiciones Generales <=== */
#define NAME_FILE_PRODUCTOR         "pg2000.txt"
#define NAME_FILE_CONSUMIDOR        "DatosConsumidor.txt"
#define N                           16
#define MAX_LENGTH                  80

//#define ON_BUFFER_TABLE     //Habilitarlo para imprimir la tabla del buffer
#define ON_CONSUMIDOR       //Permite habilitar/deshabilitar el consumidor

//#define ENABLE_DEBUG

/* ========================================================================= */


/* Variables */
pthread_mutex_t mutexA = PTHREAD_MUTEX_INITIALIZER; // Inicialización del mutex para sincronización (Productores)

char buffer[N][MAX_LENGTH];                         // Buffer para almacenar las líneas del archivo
int buff_indexIn = 0;                               // Indice donde isertar al buffer
int buff_indexOut = 0;                              // Indice donde leer del buffer
int buff_count = 0;                                 // Contador de elementos en el buffer
FILE *fp;

typedef struct{
    int onConsumidor:1;
    int onRead1:1;
    int onRead2:1;
    int turn:1;
    int endProd:1;
} t_flags;
t_flags flags;


/* Funciones para los Threads */
void * prod1(void * arg);                           // Funcion productor 1
void * prod2(void * arg);                           // Funcion productor 2
void * consumidor(void * arg);

/* Funciones generales*/
void addNameProductor(int index, char * str);       // Funcion para agregar identificador "P1:" o "P2:"
void str2Buffer(char * str);                        // Funcion para copiar cadena del buffer
void switchTurn(int sig);                           // Manejador de señales
void endThreads(int sig);                           // Manejador de señales

#ifdef ON_BUFFER_TABLE
void printBuffer();                                 // Funcion para impresión del buffer
#endif

/* DEBUG */
#ifdef ENABLE_DEBUG
#define DEBUG(str1, msj)(printf("[%s] %s\n", str1, msj))
#else
#define DEBUG(str1, msj)(NULL)
#endif








/* [=============================================================> MAIN PRINCIPAL */
int main(int argc, char * argv[]){

    signal(SIGUSR1, switchTurn);
    signal(SIGTERM, endThreads);
    
    //Permite eliminar los recursos del mutex con el parametro 'clean'
    if(argc == 2 && strcmp(argv[1], "clean") == 0){
        printf("==> Eliminando todos los recursos del Mutex...\n");
        pthread_mutex_destroy(&mutexA); 
        printf("==> Archivos eliminados, vuelve a ejecutar el compilado!\n");
        return(0);
    } 
    
    //Leo el archivo.
    fp = fopen(NAME_FILE_PRODUCTOR, "r");
    if(fp == NULL){
        printf("[ERROR] No se pudo abrir el archivo '%s' en el main\n", NAME_FILE_PRODUCTOR);
        abort();
    }

    flags.onRead1 = 0;
    flags.onRead2 = 0;
    flags.turn = 0;
    flags.endProd = 0;

    #ifdef ON_CONSUMIDOR
    flags.onConsumidor = 1;
    #else
    flags.onConsumidor = 0;
    #endif

    pthread_t p1, p2, c1;
    pthread_create(&p1, NULL, prod1, NULL);     // Creación de hilos
    pthread_create(&p2, NULL, prod2, NULL);   
    pthread_create(&c1, NULL, consumidor, NULL);

    pthread_join(p1, NULL);                     // Espera de hilos
    pthread_join(p2, NULL);
    pthread_join(c1, NULL);

    DEBUG("MAIN", "Destruyendo mutex...");
    pthread_mutex_destroy(&mutexA);              // Eliminación del mutex

    DEBUG("MAIN", "Cerrando fichero de lectura...");
    fclose(fp);

    DEBUG("MAIN", "Finalizando script...");

    return(0);
}




/* [=============================================================> Manejador de Signals */
void switchTurn(int sig){
    flags.turn = !flags.turn;
}

void endThreads(int sig){
    flags.endProd = 1;
    flags.onConsumidor = 0;
}




/* [=============================================================> THREAD del Productor 1 */
void * prod1(void * arg){

    char c[100];

    while(flags.turn){       //flag1 = 1 --> BUCLE
        sleep(0.5);
        if(flags.endProd){
            DEBUG("P1", "Finalizado");
            return(NULL);
        }
    }

    /* ---------------------------------------------------------- ] */
    while(fgets(c, 100, fp) != NULL){

        //Bloque y leo una linea, la agrego con 'P1:' delante
        pthread_mutex_lock(&mutexA);
        addNameProductor(0, c);
        str2Buffer(c);

        #ifdef ON_BUFFER_TABLE
        system("clear");
        printBuffer();
        #endif

        pthread_mutex_unlock(&mutexA);
    
        sleep(1);

        //Finaliza si llega SIGTERM
        if(flags.endProd){
            DEBUG("P1", "Finalizado");
            return(NULL);
        }

        //Queda a la espera si es el turno del prod1
        //De igual manera, finaliza si llega SIGTERM
        while(flags.turn){       //flag1 = 1 --> BUCLE
            sleep(0.5);

            if(flags.endProd){
                DEBUG("P1", "Finalizado");
                return(NULL);
            }
        }
        

    }
    /* ---------------------------------------------------------- ] */

    //Cuando finalizo por no haber mas elementos, tambien
    //finalizo al productor 2
    flags.endProd = 1;

    //Apago al consumidor pero antes, verifico que no queden
    //datos en el bufer, cualquiera sea la mitad.
    pthread_mutex_lock(&mutexA);
    if(buff_count > 0){
        if(buff_indexOut == 0){
            //Primer mitad
            flags.onRead1 = 1;
            flags.onRead2 = 0;
        } else if(buff_indexOut == N/2){
            //Segunda mitad
            flags.onRead1 = 0;
            flags.onRead2 = 1;
        }
    }
    flags.onConsumidor = 0;
    pthread_mutex_unlock(&mutexA);

    DEBUG("P1", "Finalizado");

    return(NULL);
}






/* [=============================================================> THREAD del Productor 2 */

void * prod2(void * arg){

    char c[100];

    while(!flags.turn){       //flag1 = 1 --> BUCLE
        sleep(0.5);
        if(flags.endProd){
            DEBUG("P2", "Finalizado");
            return(NULL);
        }
    }

    /* ---------------------------------------------------------- ] */
    while(fgets(c, 100, fp) != NULL){

        //Bloque y leo una linea, la agrego con 'P2:' delante
        pthread_mutex_lock(&mutexA);
        addNameProductor(1, c);
        str2Buffer(c);

        #ifdef ON_BUFFER_TABLE
        system("clear");
        printBuffer();
        #endif

        pthread_mutex_unlock(&mutexA);

        sleep(1);

        //Finaliza si llega SIGTERM
        if(flags.endProd){
            DEBUG("P2", "Finalizado");
            return(NULL);
        }
        
        //Queda a la espera si es el turno del prod1
        //De igual manera, finaliza si llega SIGTERM
        while(!flags.turn){       //flag1 = 0 --> BUCLE
            sleep(0.5);

            if(flags.endProd){
                DEBUG("P2", "Finalizado");
                return(NULL);
            }
        }
        
    }
    /* ---------------------------------------------------------- ] */
    
    flags.endProd = 1;    

    pthread_mutex_lock(&mutexA);
    if(buff_count > 0){
        if(buff_indexOut == 0){
            //Primer mitad
            flags.onRead1 = 1;
            flags.onRead2 = 0;
        } else if(buff_indexOut == N/2){
            //Segunda mitad
            flags.onRead1 = 0;
            flags.onRead2 = 1;
        }
    }
    flags.onConsumidor = 0;
    pthread_mutex_unlock(&mutexA);

    DEBUG("P2", "Finalizado");
    return(NULL);
}





void addNameProductor(int index, char * str){
    char aux[MAX_LENGTH];
    if(!index){
        strcpy(aux, "P1:");
    } else {
        strcpy(aux, "P2:");
    }
    strcat(aux, str);
    strcpy(str, aux);
}



// Agrega un string al buffer, ademas verifica si esta lleno
// alguna de las mitades y decide en consecuencia.
// Lee las variables:
//  - buffer
//  - buff_indexIn
//  - buff_count
//  - buff_indexOut
// Por lo que estas variables deberan usar mutex
void str2Buffer(char * str){    // [--> SIEMPRE SE LLAMA DESPUES DE UN MUTEX_LOCK <--]
    
    //Inserto donde apunta el indice [IN]
    strcpy(&buffer[buff_indexIn][0], str);

    //Inserto sin importar si es la primer mitad o la segunda mitad.
    buff_indexIn++;
    if(buff_indexIn >= N){
        buff_indexIn = 0;
    }

    //Aumento un contador
    if(buff_count < N){
        buff_count++;
    }

    //Verifico si alguna de las mitades esta llena
    if(buff_count > N/2){
        if(buff_indexIn != buff_indexOut){      //No se solapan
            if(buff_indexOut == 0){
                //Primer mitad lista
                flags.onRead1 = 1;
                flags.onRead2 = 0;
            } else if(buff_indexOut == N/2){
                //Segunda mitad lista
                flags.onRead1 = 0;
                flags.onRead2 = 1;
            }
        } else {                                //Se solapan
            if(buff_indexOut == 0){
                //Primer mitad lista
                flags.onRead1 = 0;
                flags.onRead2 = 1;
            } else if(buff_indexOut == N/2){
                //Segunda mitad lista
                flags.onRead1 = 1;
                flags.onRead2 = 0;
            }
        }
    }
}





#ifdef ON_BUFFER_TABLE
void printBuffer(){

    printf("  N: [%d]   MAX_LENGTH:[%d]   PID: [%d]   Turn: [%d]\n", N, MAX_LENGTH, getpid(), flags.turn);
    printf("  Buffer Circular: Cant: [%d]  IN/OUT:[%d/%d]\n\n", buff_count, buff_indexIn, buff_indexOut);

    for(int i = 0; i < N/2; i++){   //Recorre los indices
        if(i < 10)
            printf(" [ %d] [", i);
        else
            printf(" [%d] [", i);

        //Primer mitad (Recorre el ancho)
        for(int j = 0; j < MAX_LENGTH; j++){
            if(isalnum(buffer[i][j]) || buffer[i][j] == ':'){
                printf("%c", buffer[i][j]);
            } else {
                printf(" ");
            }

            if(j < MAX_LENGTH - 1){
                printf(" ");
            } else {
                //printf("]\n");
                printf("]");
            }
        }

        printf("       [");

        //Segunda mitad (Recorre el ancho)
        for(int j = 0; j < MAX_LENGTH; j++){
            if(isalnum(buffer[i + (N/2)][j]) || buffer[i + (N/2)][j] == ':'){
                printf("%c", buffer[i + (N/2)][j]);
            } else {
                printf(" ");
            }

            if(j < MAX_LENGTH - 1){
                printf(" ");
            } else {
                printf("]\n");
            }
        }
    }

    printf("\n\n");
}
#endif








void * consumidor(void * arg){
    
    printf("  N: [%d]   MAX_LENGTH:[%d]   PID: [%d]\n", N, MAX_LENGTH, getpid());
    while(1){
        
        usleep(1000);

        pthread_mutex_lock(&mutexA);

        if(flags.onRead1){
            flags.onRead1 = 0;
            
            //Abro el archivo
            FILE * fp_cons = fopen(NAME_FILE_CONSUMIDOR, "a");
            if(fp_cons == NULL){
                printf("[ERROR:C] No se pudo abrir el archivo '%s' en el thread consumidor\n", NAME_FILE_CONSUMIDOR);
                abort();
            }   

            //Leo primer mitad
            fprintf(fp_cons, "-----------------------> PRIMER MITAD <---\n");
            while(buff_indexOut < N/2){
                fputs(buffer[buff_indexOut], fp_cons);

                #ifndef ON_BUFFER_TABLE
                printf("[C1:1] Leyo: \"");
                #endif

                for(int k = 0; k < MAX_LENGTH; k++) {
                    #ifndef ON_BUFFER_TABLE
                    if(isalnum(buffer[buff_indexOut][k]) || buffer[buff_indexOut][k] == ':'){
                        printf("%c", buffer[buff_indexOut][k]);
                    } else {
                        printf(" ");
                    }
                    #endif
                    buffer[buff_indexOut][k] = '\0';
                }

                #ifndef ON_BUFFER_TABLE
                printf("\"\n");
                fflush(stdout);
                #endif

                buff_indexOut++;
                
                buff_count--;
                if(buff_count == 0) {       //Decrementa el contador de elementos, pero si
                    fprintf(fp, "\n");      //llega a cero, entonces hace un 'break'. (Esto porque
                    break;                  //si quedan antes de cerrar el consumidor, leera los que
                }                           //existan unicamente)
                  
            }
            fprintf(fp_cons, "==========================================\n");
            fclose(fp_cons);
            

        } else if(flags.onRead2){
            flags.onRead2 = 0;

            FILE * fp_cons = fopen(NAME_FILE_CONSUMIDOR, "a");
            if(fp_cons == NULL){
                printf("[ERROR:C] No se pudo abrir el archivo '%s' en el thread prod2\n", NAME_FILE_CONSUMIDOR);
                abort();
            } 

            //Leo segunda mitad
            fprintf(fp_cons, "-----------------------> SEGUNDA MITAD <---\n");
            while(buff_indexOut > 0){          
                fputs(buffer[buff_indexOut], fp_cons);
                
                #ifndef ON_BUFFER_TABLE
                printf("[C1:2] Leyo: \"");
                #endif

                for(int k = 0; k < MAX_LENGTH; k++) {
                    #ifndef ON_BUFFER_TABLE
                    if(isalnum(buffer[buff_indexOut][k]) || buffer[buff_indexOut][k] == ':'){
                        printf("%c", buffer[buff_indexOut][k]);
                    } else {
                        printf(" ");
                    }
                    #endif
                    buffer[buff_indexOut][k] = '\0';
                }

                #ifndef ON_BUFFER_TABLE
                printf("\"\n");
                fflush(stdout);
                #endif
                    

                buff_indexOut++;                //Incrementa el indice donde lee, pero si llega al
                if(buff_indexOut >= N)          //final, entonces vuelve al primer indice. (Buffer Circular)
                    buff_indexOut = 0;

                buff_count--;
                if(buff_count == 0) {           //Decrementa el contador de elementos, pero si
                    fprintf(fp_cons, "\n");          //llega a cero, entonces hace un 'break'. (Esto porque
                    break;                      //si quedan antes de cerrar el consumidor, leera los que
                }                               //existan unicamente)
                    
                
            }
            fprintf(fp_cons, "==========================================\n");
            fclose(fp_cons);

        }

        if(!flags.onConsumidor){
            break;  
        }

        pthread_mutex_unlock(&mutexA);
    }

    DEBUG("C1", "Finalizado");
    return(NULL);
}






/* Comprobacion de Errores */
#if N % 2 != 0
#error N debe ser Par!
#endif

