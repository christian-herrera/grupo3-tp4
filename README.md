# SOyR - Trabajo Practico Nro. 4

![Static Badge](https://img.shields.io/badge/status-stable-green)
![Static Badge](https://img.shields.io/badge/version-1.2-blue)


## Enunciado
Se desea implementar un buffer PING-PONG para que dos procesos generen datos y otro los consuma. La implementación debe utilizar un archivo para la obtención de datos producidos y otro para almacenar los datos leídos.

 - [x] Los procesos productores deben leer los datos desde un archivo y escribirlos en un buffer ping-pong, en donde cada dato será una línea de texto.
 - [x] Para diferenciar el texto producido por cada uno la cadena debe iniciar con P1: oP2: según quien sea el que generó el dato. El productor deberá leer las líneas del archivo que contengan texto y producir de a una línea por segundo.
 - [x] Solo deberá ejecutarse un productor a la vez, para cambiar de proceso productor se deberá enviar una señal que permitirá pasar de un productor a otro.
 - [x] El proceso consumidor debe leer desde el buffer ping-pong e imprimir los valores leídos en pantalla. Además, debe almacenarlos en otro archivo.

El problema debe implementarse usando hilos, variables compartidas y mutex.


## Avance
Implementado con Hilos y Mutex.
 - [x] Los dos Hilos logran leer de un mismo archivo.
 - [x] Cada hilo lee una linea, le agrega la leyenda `P1:` o `P2:` segun el numero del productor y lo carga al buffer.
 - [x] Cada insercion en el buffer se hace a razon de uno por segundo.
 - [x] El algoritmo detecta que se llego a `N/2` elementos.
 - [x] El consumidor va leyendo los elementos.
 - [x] Solo se ejecuta un productor a la vez.
 - [x] El buffer es personalizable, tanto en numero de elementos (`N`) como en cantidad de caracteres por elementos (`MAX_LENGTH`).
 - [x] El cambio de productor es realizable con la señal `SIGUSR1`.


## Bugs Corregidos

 - `#1`: Se agrego un delay dentro del while del consumidor, la correccion hace que el CPU no este al 100% ya que sin ese delay, el while queda sin ejecutar nada, y eleva el uso del CPU al maximo.
 - `#2`: Se agregaron las lineas de codigo para pasar el parametro `clean` y limpiar todos los recursos del mutex. Esto se realizo porque si se detiene el proceso cuando esta en ejecucion, no se logran eliminar dichos recursos.


## Funcionamiento
Primero compilar el archivo `Ej11.c` con el comando:

	gcc -o t Ej11.c -lpthread

Luego, al momento de ejecutarlo con `./t` e ir enviandole señales `SIGUSR1`, se obtiene un resultado como el siguiente:

![](/assets/video_demo.webm){width="100%"}

Mientras que al leer la salida del consumidor, es decir, el archivo `DatosConsumidor.txt`, se obtiene la lista de los `N/2` elementos leidos por cada ejecucion del consumidor. Esto se aprecia en la siguiente imagen:

<div align="center">
	<img src="/assets/screenshot.png" alt="Imagen no disponible!">
</div>